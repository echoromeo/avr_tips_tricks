# AVR bare-metal project examples

## Purpose
- Have a starting point with stuff you "always" end up adding anyways
- Cheat sheet to learn/remember how to use certain easy or elegant solutions
- Some small functions to simplify certain operations

## Architectures
At the moment only the new tinyAVR/megaAVR 0/1-series are supported, but this is very similar to xmega

## IDE & Compiler
Centered around Atmel Studio and AVR-GCC at the moment

## Future
- TODO: Add xmega and legacy mega versions
- TODO: Add VSCode workspace and makefile example

Do you have any suggestions? Add an issue and let me know!