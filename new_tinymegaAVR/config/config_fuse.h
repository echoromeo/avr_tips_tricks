/*
 * new_tinymegaAVR fuse settings
 *
 * Might differ a bit between devices in the families
 */

#ifndef _CONFIG_FUSE_H_
#define _CONFIG_FUSE_H_
#include <avr/io.h>
#include <avr/fuse.h>

/* Defines for each setting in case these will be used by other parts of the code at compile time */
#define FUSECFG_WDTCFG      0x00
#define FUSECFG_BODCFG      (SLEEP_SAMPLED_gc | ACTIVE_ENABLED_gc | SAMPFREQ_1KHZ_gc | LVL_BODLEVEL3_gc)
#define FUSECFG_OSCCFG      FREQSEL_20MHZ_gc
#define FUSECFG_TCD0CFG     0x00
#define FUSECFG_SYSCFG0     (CRCSRC_NOCRC_gc | RSTPINCFG_RST_gc)
#define FUSECFG_SYSCFG1     SUT_64MS_gc
#define FUSECFG_APPEND      0x00
#define FUSECFG_BOOTEND     0x00

/* The values to put in the elf file */
FUSES = {
	.WDTCFG =   FUSECFG_WDTCFG,
	.BODCFG =   FUSECFG_BODCFG,
	.OSCCFG =   FUSECFG_OSCCFG,
	.SYSCFG0 =  FUSECFG_SYSCFG0,
	.SYSCFG1 =  FUSECFG_SYSCFG1,
	.APPEND =   FUSECFG_APPEND,
	.BOOTEND =  FUSECFG_BOOTEND
};

#endif // _CONFIG_FUSE_H_