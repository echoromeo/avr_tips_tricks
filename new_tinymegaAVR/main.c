/*
 * new_tinymegaAVR main
 *
 * Made for tinyAVR/megaAVR 0/1-series
 * Configured for and tested on ATmega4809
 */

/* F_CPU is best to define in project properties
 * (Alt-F7) -> Toolchain -> AVR/GNU C Compiler -> Symbols
 *
 * If using both Debug and Release configurations you
 * must remember to modify symbols for both.
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <stdbool.h>
#include "config/config_fuse.h"
#include "config/config_eeprom_user_row.h"

/* The tribyte variable is often very good to have */
typedef __uint24 uint24_t;

/* Example of status "register" variable
 * and how to map to GP registers
 */
volatile union {
	uint8_t reg; // The full status byte
	struct  { // The bitfields
		uint8_t buttonpress     : 1;
		uint8_t twobitvalue     : 2;
		uint8_t reserved        : 5;
	};
} status __attribute__((io(&GPIOR0)));

int main(void)
{
	/* CPU clock is 3,33 MHz by default, configure MCLKCTRLB to change it */
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB,
	                 CLKCTRL_PDIV_2X_gc | CLKCTRL_PEN_bm); //10 MHz
//	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, 0);                                    //20 MHz, max

	/* Example use of eeprom and user row pointers */
	if(eeprom->data == 0x01) {
		eeprom->data++;
		eeprom_userrow_write_buffer(true);
	}

	if(userrow->calibration == 0x01) {
		PORTA.OUTTGL = 0xff;
	}

	if(eeprom->data == 0x02) {
		PORTA.DIRTGL = 0xff;
	}

	/* Example use of sleep config */
	set_sleep_mode(SLEEP_MODE_IDLE);

	/* Example uses of pin interrupts and status register*/
	PORTB.DIRCLR = PIN2_bm; /* SW0 on 4809 XPRO */
	PORTB.PIN2CTRL = PORT_ISC_FALLING_gc | PORT_PULLUPEN_bm;
	PORTB.DIRSET = PIN5_bm; /* LED0 on 4809 XPRO */

	/* Remember to enable interrupts! */
	sei();

	while(1) {
		if(status.buttonpress) {
			status.buttonpress = false;

			/* Toggle LED0 on 4809 XPRO */
			PORTB.OUTTGL = PIN5_bm;
		}

		/* Example use of sleep */
		sleep_mode();
	}
}

/* Example ISR */
ISR(PORTB_PORT_vect)
{
	status.buttonpress = true;

	// Clear pin interrupt flag(s)
	PORTB.INTFLAGS = PIN2_bm;
}
